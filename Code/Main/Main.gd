extends Node
class_name Main
@onready var tile_map = $TileMap

func _process(delta):
	if Input.is_action_just_pressed("dig_block"):
		var map_coords = tile_map.local_to_map(tile_map.get_local_mouse_position())
		tile_map.erase_cell(0, map_coords)
	
	if Input.is_action_just_pressed("place_block"):
		var map_coords = tile_map.local_to_map(tile_map.get_local_mouse_position())
		tile_map.set_cell(0, map_coords,0,Vector2i(0,1),0)
